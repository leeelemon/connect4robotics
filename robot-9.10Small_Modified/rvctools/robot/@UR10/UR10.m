classdef UR10 < handle
    properties
        %> Robot model
        model;
        
        %>
        workspace = [-2 2 -2 2 0 2];   
    end
    
    methods%% Class for UR10 robot simulation
function self = UR10()

%> Define the boundaries of the workspace

        
% robot = 
self.GetUR10Robot();
% robot = 
self.PlotAndColourRobot();%robot,workspace);
end

%% GetUR10Robot
% Given a name (optional), create and return a UR10 robot model
function GetUR10Robot(self)
%     if nargin < 1
        % Create a unique name (ms timestamp after 1ms pause)
        pause(0.001);
        name = ['UR_10_',datestr(now,'yyyymmddTHHMMSSFFF')];
%     end

%     L0 = Link('sigma',1,'d',0.128,'a',0,'q',0);%,'alpha',-pi/2);
    L1 = Link('d',0.128,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
    L2 = Link('d',0.176,'a',0.6129,'alpha',-pi,'offset',-pi/2,'qlim',[deg2rad(-90),deg2rad(90)]);
    L3 = Link('d',0.12781,'a',0.5716,'alpha',pi,'offset',0,'qlim',[deg2rad(-170),deg2rad(170)]);
    L4 = Link('d',0.1157,'a',0,'alpha',-pi/2,'offset',-pi/2,'qlim',[deg2rad(-360),deg2rad(360)]);
    L5 = Link('d',0.1157,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
    L6 = Link('d',0,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-360),deg2rad(360)]);
  %  L7 = Link('d',0,'a',0.2,'alpha',0,'offset',0,'qlim',[deg2rad(0),deg2rad(0)]);

    self.model = SerialLink([L1 L2 L3 L4 L5 L6],'name',name);
end
% r.base = trotz(-pi/2) * trotx(pi);

% tr_x_p90 = makehgtform('xrotate',pi/2); tr_x_p90 = tr_x_p90(1:3,1:3);
% tr_y_p90 = makehgtform('yrotate',pi/2); tr_y_p90 = tr_y_p90(1:3,1:3);
% tr_z_p90 = makehgtform('zrotate',pi/2); tr_z_p90 = tr_z_p90(1:3,1:3);
% 
% tr_x_n90 = makehgtform('xrotate',-pi/2); tr_x_n90 = tr_x_n90(1:3,1:3);
% tr_y_n90 = makehgtform('yrotate',-pi/2); tr_y_n90 = tr_y_n90(1:3,1:3);
% tr_z_n90 = makehgtform('zrotate',-pi/2); tr_z_n90 = tr_z_n90(1:3,1:3);
% 
% % arm = load('UR_10_lowResolution.mat');
% 
% % for i = 1: r.n+1
% %     r.faces{i} = arm.shapeModel(i).face;
% %     r.points{i} = arm.shapeModel(i).vertex;
% % end
% 
% r.delay = 0;
% 
% % Check if there is a robot with this name, otherwise plot one 
% % if isempty(findobj('Tag', r.name))
% %     r.plot3d([0,0,pi/2,0,0,0]);
% %     if isempty(findobj(get(gca,'Children'),'Type','Light'))
% %         camlight
% %     end
% % end
% 
% r.plot([0,0,0,0,0,0],'scale',0.6,'nowrist');
% 
% r.delay = 0;
% % handles = findobj('Tag', r.name);
% % h = get(handles,'UserData');
% % h.link(1).Children.FaceColor = [0.7,0.7,0.7];
% % h.link(2).Children.FaceColor = [1, 1, 0.8];
% % h.link(3).Children.FaceColor = [1, 1, 0.9];
% % h.link(4).Children.FaceColor = [1, 1, 0.9];
% % h.link(5).Children.FaceColor = [0.8,0.8,0.8];
% % h.link(6).Children.FaceColor = [0.8,0.8,0.8];
% % h.link(7).Children.FaceColor = [0.8,0.8,0.8];

%% PlotAndColourRobot
% Given a robot index, add the glyphs (vertices and faces) and
% colour them in if data is available 
function PlotAndColourRobot(self)%robot,workspace)
    for linkIndex = 0:self.model.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['UR10Link',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
        self.model.faces{linkIndex+1} = faceData;
        self.model.points{linkIndex+1} = vertexData;
    end

    % Display robot
    self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
    if isempty(findobj(get(gca,'Children'),'Type','Light'))
        camlight
    end  
    self.model.delay = 0;

    % Try to correctly colour the arm (if colours are in ply file data)
    for linkIndex = 0:self.model.n
        handles = findobj('Tag', self.model.name);
        h = get(handles,'UserData');
        try 
            h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                          , plyData{linkIndex+1}.vertex.green ...
                                                          , plyData{linkIndex+1}.vertex.blue]/255;
            h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
    end
end        
    end
end