function buttonPlot
% Create a UI figure window
fig = uifigure;

% Create a UI axes
ax = uiaxes('Parent',fig,...
            'Units','pixels',...
            'Position', [104, 123, 300, 201]);   

% Create a push button
btn = uibutton(fig,'push',...
               'Position',[420, 218, 100, 22],...
               'ButtonPushedFcn', @(btn,event) plotButtonPushed(btn,ax));
end

% Create the function for the ButtonPushedFcn callback
function plotButtonPushed(btn,ax)
        stop = 1;
        dobot.plot([0,0,0,0,0])
end  

function main
clear all; close all; clf; clc;
global g;
setupgame;
for n=1:g.numgames
    setUpRobot;
    initboard;
    plotply;
    showboard;
    while ~g.done
        getmove;
        checkwin;
        showboard;
    end 
    pause(1)
end

function setUpRobot
global g;
global dobot;
L1 = Link('d',0.103,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)])
L2 = Link('d',0,'a',0.14363,'alpha',0,'offset',0,'qlim',[deg2rad(5), deg2rad(80)])
L3 = Link('d',0,'a',0.17063,'alpha',0,'offset',0,'qlim',[deg2rad(-80), deg2rad(5)])
L4 = Link('d',0,'a',0.05109,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)])
L5 = Link('d',0,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)])

dobot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot')
%dobot.tool = transl(0,0,0.03);
qz = [0 0 0 0 0];
T = transl(0,0,0.1);
Q = ikcon(dobot,T)
figure(1);
dobot.plot(qz);
hold on;

function dobotTurn 
global g; 
global dobot;
[r,c]=ind2sub(size(g.board),g.lastmove)
offset = 0.033;
switch c 
    case 1
       dropPos =  transl(0.3, offset*-3, 0.103)*trotx(pi);
    case 2
       dropPos =  transl(0.3, offset*-2, 0.103)*trotx(pi);
    case 3
       dropPos = transl(0.3, offset*-1, 0.103)*trotx(pi);
    case 4 
        dropPos = transl(0.3, offset*0, 0.103)*trotx(pi);
    case 5 
        dropPos = transl(0.3, offset*1, 0.103)*trotx(pi);
    case 6 
        dropPos = transl(0.3, offset*2, 0.103)*trotx(pi);
    case 7 
        dropPos = transl(0.3, offset*3, 0.103)*trotx(pi);
end 
DropPosition = dobot.ikcon(dropPos)
currentPose = dobot.getpos
path = jtraj(currentPose, DropPosition, 30);
for i = 1:length(path)
    dobot.animate(path(i,1:5));
    drawnow;
    pause(0.01);
end
pause;

function setupgame
global g;
prompts = {'Player 1''s name','Player 2''s name','# rows','# columns','connect # to win','# games'};
inp = {'Orange','*Black','6','7','2','1'};
inp = inputdlg(prompts,'Connect X',1,inp);
if isempty(inp); error('User canceled input'); end;
g.name{1}=inp{1};           % Player 1
g.name{2}=inp{2};           % Player 2
g.nr=str2double(inp{3});    % # of rows
g.nc=str2double(inp{4});    % # of columns
g.n=str2double(inp{5});     % connect 4 or 3 or 2?
g.numgames=str2double(inp{6});  
g.playercolors={'y','r'}; % yellow,red
g.results = [0 0 0];
 
function initboard
global g;
g.board = zeros(g.nr,g.nc); % playing board
g.player = 1; % current player
g.done = 0;
g.winner = 0;
g.lastmove = 0;
%% checker positions in model 
%checkerpose stores all the poses of the checkers positions for modelling
%purposes
%Bottom left is 0,0
dist = 0.033 % distance between centre of holes 0.034
g.checkerpose = ones(6,7,2) 
x = 0;
y = 0;
for n=6:-1:1
    x = 0;
    for m=1:1:7   
        g.checkerpose(n,m,1) = (x) * dist;
        g.checkerpose(n,m,2) = (y) * dist;
        x = x + 1;   
    end
    y = y + 1; 
end
g.checkerpose = flipud(g.checkerpose)   %I flip it horizontally because the connect4 code stores the board differently

function plotply
global g;
%% Plot the ply model board 
figure(1)
g.boardloc = transl(0,0,0); % pick a location later
[fboard,vboard,databoard] = plyread('board.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.boardloc(1,4),vboard(:,2) + g.boardloc(2,4), vboard(:,3) + g.boardloc(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
camlight;
hold on;
%% Checker locations
checkerloc = zeros(7,3,2)
checkerloc(:,:,1) = [3.7,3.7,3.7;16.2,16.2,16.2;28.7,28.7,28.7;41.2,41.2,41.2;53.7,53.7,53.7;66.2,66.2,66.2;78.7,78.7,78.7]
checkerloc(:,:,2) = [17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;]
checkerloc = checkerloc * 10^-3
%% Plot the checkers
% Yellow Holder
g.yellowholder = transl(0.5,0.5,0) * trotz(deg2rad(90)); % pick a location later
[fboard,vboard,databoard] = plyread('checkerholder.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.yellowholder(1,4),vboard(:,2) + g.yellowholder(2,4), vboard(:,3) + g.yellowholder(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
% Red Holder
g.redholder = transl(-0.2,0.2,0) * trotz(deg2rad(90)); % pick a location later
[fboard,vboard,databoard] = plyread('checkerholder.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.redholder(1,4),vboard(:,2) + g.redholder(2,4), vboard(:,3) + g.redholder(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
% red checkers
[fchecker,vchecker,datachecker] = plyread('redchecker.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
for n=1:1:7
    for m=1:1:3 
        boardMesh_h = trisurf(fchecker,vchecker(:,1) + g.redholder(1,4) + checkerloc(n,m,1) ,vchecker(:,2) + g.redholder(2,4) + checkerloc(n,m,2) , vchecker(:,3) + g.redholder(3,4) ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
    end
end
%yellow checkers
[fchecker,vchecker,datachecker] = plyread('yellowchecker.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
for n=1:1:7
    for m=1:1:3 
        boardMesh_h = trisurf(fchecker,vchecker(:,1) + g.yellowholder(1,4) + checkerloc(n,m,1) ,vchecker(:,2) + g.yellowholder(2,4) + checkerloc(n,m,2) , vchecker(:,3) + g.yellowholder(3,4) ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
    end
end
%% Plot a dummy checker for test
% [fchecker,vchecker,datachecker] = plyread('redchecker.ply','tri');
% % Scale the colours to be 0-to-1 (they are originally 0-to-255
% vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% % Then plot the trisurf
% boardMesh_h = trisurf(fchecker,vchecker(:,1) + g.checkerpose(1,1,1) ,vchecker(:,2) + g.checkerpose(1,1,2), vchecker(:,3) + g.boardloc(3,4) ...
%     ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
function showboard
figure(2)
global g;
if g.lastmove==0 % board initialization
    clf reset
    axis([0 g.nc+1 0 g.nr+1])
    hold on
    bc='k-';
    plot([.5 g.nc+.5],[.5 .5],bc); % bottom
    for i=0:g.nc
        plot([i+.5 i+.5],[.5 g.nr+.5],bc); % vertical lines
    end
else % plot last move
    [r,c]=ind2sub(size(g.board),g.lastmove);
    i = g.board(r,c);
    plot(c,r,'o','markersize',250/max(g.nr,g.nc),'markeredgecolor','black','markerfacecolor',g.playercolors{i})
end
if g.winner>0
    plot(g.win_x,g.win_y,'k-','linewidth',5,'color','green');
    txt = sprintf('%s wins',g.name{g.winner});
    updateresults(g.winner);
    playdonesound;
    %call function for victory dance here
elseif g.done>0
    txt = sprintf('Tie game!');
    updateresults(3);
    playdonesound;
else
    txt = sprintf('%s''s turn',g.name{g.player});
end
title(sprintf('Connect %u : %s',g.n,txt),'fontsize',16);
drawnow
 
function getmove
global g;
goodmove = 0;
while ~goodmove
    if strncmp(g.name{g.player},'*',1)
        c = getcomputermove;
    else
        [x,y] = ginput(1);
        c = round(x);
    end
    if c<1 || c>g.nc || g.board(g.nr,c)>0
        beep;
        continue;
    end
    goodmove=1;
    r=find(g.board(:,c)==0);
    g.board(r(1),c) = g.player;
    %call function to pick up checker and drop it here
    plotchecker(r,c);
    dobotTurn;
    g.lastmove = sub2ind(size(g.board),r(1),c);
    g.player=mod(g.player,2)+1;
end
 
function checkwin
global g;
offs(1) = g.nr; % right
offs(2) = 1; % up
offs(3) = g.nr+1; %up, right
offs(4) = -g.nr+1; %up, left
for s=1:g.nr*g.nc
    if g.board(s)>0
        [r,c]=ind2sub(size(g.board),s);
        for o=1:length(offs)
            if o>1 && r>(g.nr-g.n+1); continue; end; % don't do upward check
            if (o==1 || o==3) && c>(g.nc-g.n+1); continue; end; % don't do rightward check
            if o==4 && c<g.n; continue; end; % don't do leftward check
            i=(0:g.n-1)*offs(o)+ s;
            if isempty(find(i<1, 1)) && isempty(find(i>g.nr*g.nc, 1))
                t=g.board(i);
                if t(:)==t(1)
                    g.winner=t(1);
                    g.done=1;
                    [r1 c1] = ind2sub(size(g.board),i(1));
                    [r2 c2] = ind2sub(size(g.board),i(g.n));
                    g.win_x = [c1 c2];
                    g.win_y = [r1 r2];
                    return
                end
            end
        end
    end
end
g.done = ( nnz(g.board)==(g.nr*g.nc) );
 
function playdonesound
global g;
if sum(g.results)<g.numgames; return; end
builtinsounds = {'train', 'gong', 'chirp', 'handel', 'laughter', 'splat'};
i=randi(length(builtinsounds));
load(builtinsounds{i});
wavplay(y,Fs,'async')
 
function updateresults(p)
global g;
g.results(p)=g.results(p)+1;
fprintf('%s wins: %u\n%s wins: %u\nTies:%u\n',g.name{1},g.results(1),g.name{2},g.results(2),g.results(3));
 
function c = getcomputermove
global g;
o = find(g.board(g.nr,:)==0);
i = randi(length(o));
c = o(i);

function plotchecker(r,c)
global g;
figure(1)
if g.player == 2
    [fchecker,vchecker,datachecker] = plyread('redchecker.ply','tri');
else 
    [fchecker,vchecker,datachecker] = plyread('yellowchecker.ply','tri');
end
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fchecker,vchecker(:,2) + g.checkerpose(r(1),c,1) ,vchecker(:,1) + g.boardloc(3,4) , vchecker(:,3) + g.checkerpose(r(1),c,2) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
%how do i rotate these checkers??
drawnow;

      
           
           
