
clf
clc
clear all
% Link('theta',__,'d',__,'a',__,'alpha',__,'offset',__,'qlim',[ ... ])

L1 = Link('d',0.103,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)])

L2 = Link('d',0,'a',0.14363,'alpha',0,'offset',pi/2,'qlim',[deg2rad(5), deg2rad(80)])

L3 = Link('d',0,'a',0.17063,'alpha',0,'offset',0,'qlim',[deg2rad(-5), deg2rad(190)])

L4 = Link('d',0,'a',0.05109,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)])

L5 = Link('d',0,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)])

dobot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot')

dobot.tool = transl(0,0,-0.03);

qz = [0 0 0 0 0];

T = transl(0.3,0.3,0.1);

Q = ikcon(dobot,T)

% figure(2)
dobot.plot(Q)










%%
% dobot.plot(q);
% %% ghgghg
% for i=0:-2:-70 %joint 1 angle limits -170 to 170 
%     q2 = i*pi/180;
%     for j=-135:30:90 %joint 2 angle limits -90 to 135 
%         q3 = j*pi/180;
%         q4 = (q2+q3)*-1;
%         q = [0 q2 q3 q4 0];
%         dobot.plot(q);
%     end
% end
% 
% %dobot.plot(q)

dobot.plotopt = {'nojoints'};
dobot.teach()

%% plot ellipsoids
% centerPoint = [0,0,0];
% radii = [0.05,0.1,0.05];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% for i = 1:6
%     dobot.points{i} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{i} = delaunay(dobot.points{i});    
%     warning on;
% end

%experimenting
%base
% centerPoint = [0,0,0];
% radii = [0.005,0.005,0.005];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{1} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{1} = delaunay(dobot.points{1});    
%     warning on;
%     
% %upright rotator
% centerPoint = [0,0,0];
% radii = [0.08,0.157,0.175];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{2} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{2} = delaunay(dobot.points{2});    
%     warning on;
%     
% %upper arm
% centerPoint = [0,0,0];
% radii = [0.157,0.05,0.036];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{3} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{3} = delaunay(dobot.points{3});    
%     warning on;
%     
% %lower arm
% centerPoint = [0,0,0];
% radii = [0.157,0.05,0.036];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{4} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{4} = delaunay(dobot.points{4});    
%     warning on;
%     
% %end bit
% centerPoint = [0,0,0];
% radii = [0.07,0.017,0.025];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{5} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{5} = delaunay(dobot.points{5});    
%     warning on;
%     
% %end spinny bit
% centerPoint = [0,0,0];
% radii = [0.07,0.017,0.025];
% [X,Y,Z] = ellipsoid( centerPoint(1), centerPoint(2), centerPoint(3), radii(1), radii(2), radii(3) );
% dobot.points{6} = [X(:),Y(:),Z(:)];
%     warning off
%     dobot.faces{6} = delaunay(dobot.points{6});    
%     warning on;
% 
% dobot.plot3d([0,0,0,0,0]);
% axis equal
% camlight

%% attempting to get ply files playing
%base
[ faceData, vertexData, plyData{0+1} ] = plyread(['0.ply'],'tri');
dobot.faces{0+1} = faceData;
dobot.points{0+1} = vertexData;

%spinny bit
[ faceData, vertexData, plyData{1+1} ] = plyread(['1.ply'],'tri');
dobot.faces{1+1} = faceData;
dobot.points{1+1} = vertexData;

%first arm
[ faceData, vertexData, plyData{2+1} ] = plyread(['2.ply'],'tri');
dobot.faces{2+1} = faceData;
dobot.points{2+1} = vertexData;

%second arm
[ faceData, vertexData, plyData{3+1} ] = plyread(['3.ply'],'tri');
dobot.faces{3+1} = faceData;
dobot.points{3+1} = vertexData;

%wrist
[ faceData, vertexData, plyData{4+1} ] = plyread(['4.ply'],'tri');
dobot.faces{4+1} = faceData;
dobot.points{4+1} = vertexData;

%end effector rotation (needs a different ply file
[ faceData, vertexData, plyData{5+1} ] = plyread(['5.ply'],'tri');
dobot.faces{5+1} = faceData;
dobot.points{5+1} = vertexData;

%attempt to colour (didnt work)
for linkIndex = 0:dobot.n
        handles = findobj('Tag', dobot.name);
        h = get(handles,'UserData');
        try 
            h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                          , plyData{linkIndex+1}.vertex.green ...
                                                          , plyData{linkIndex+1}.vertex.blue]/255;
            h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
end
dobot.plot3d([0,1,1,-0.5,0])
camlight;