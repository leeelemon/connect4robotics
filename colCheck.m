function [colCheckOutput, extraOut] = colCheck(pointOnPlane, planeNormal, i, path, dobot, x)
global g;
extraOut = g.extra;
colCheckOutput = 0;
pointOnPlaneR = pointOnPlane;
pointOnPlaneR(2) = pointOnPlaneR(2)-0.45+0.6;
pointOnPlaneE = pointOnPlane;
pointOnPlaneE(2) = pointOnPlaneE(2)-0.15;
trcol = zeros(4,4,dobot.model.n+1);
    trcol(:,:,1) = dobot.model.base;
    L = dobot.model.links;
    for j = 1 : dobot.model.n
        trcol(:,:,j+1) = trcol(:,:,j) * trotz(path(i,j)+L(j).offset) * transl(0,0,L(j).d) * transl(L(j).a,0,0) * trotx(L(j).alpha);
    end    
    for j = 1 : size(trcol,3)-1
            [intersectionPoint, check] = LinePlaneIntersection(planeNormal,pointOnPlane,trcol(1:3,4,j)',trcol(1:3,4,j+1)');
            if check ==1
                if intersectionPoint(1)<g.boardloc(1,4)+0.25 && g.boardloc(1,4)<intersectionPoint(1)
                    if intersectionPoint(3)<g.boardloc(3,4)+0.18 && g.boardloc(3,4)<intersectionPoint(3)
                        display(['Collision with board at joint ', num2str(j), ' Global co-ord ', num2str(intersectionPoint(1)),',',num2str(intersectionPoint(2)),',',num2str(intersectionPoint(3)),'.' ' See plot.'])
                        figure(1)
                        plot3(intersectionPoint(1),intersectionPoint(2),intersectionPoint(3), 'g.', 'MarkerSize', 20 );
                        colCheckOutput = 1;
                    end
                end
            end
            [intersectionPoint, check] = LinePlaneIntersection(planeNormal,pointOnPlaneR,trcol(1:3,4,j)',trcol(1:3,4,j+1)');
            if check ==1
                if intersectionPoint(1)<g.boardloc(1,4)+0.55 && g.boardloc(1,4)-0.65<intersectionPoint(1)
                    if intersectionPoint(3)<g.boardloc(3,4)+0.38 && g.boardloc(3,4)<intersectionPoint(3)
                        display(['Collision with rear wall at joint ', num2str(j), ' Global co-ord ', num2str(intersectionPoint(1)),',',num2str(intersectionPoint(2)),',',num2str(intersectionPoint(3)),'.' ' See plot.'])
                        figure(1)
                        plot3(intersectionPoint(1),intersectionPoint(2),intersectionPoint(3), 'g.', 'MarkerSize', 20 );
                        colCheckOutput = 1;
                    end
                end
            end
            if g.extra == 1
                [intersectionPoint, check] = LinePlaneIntersection(planeNormal,pointOnPlaneE,trcol(1:3,4,j)',trcol(1:3,4,j+1)');
                if check ==1
                    if intersectionPoint(1)<g.boardloc(1,4)+0.55 && g.boardloc(1,4)-0.65<intersectionPoint(1)
                        if intersectionPoint(3)<g.boardloc(3,4)+0.40 && g.boardloc(3,4)<intersectionPoint(3)
                            display(['Collision with foreign object imminent. Please remove item and press any button to continue'])
                            buttloop = 1;
                            while buttloop ==1
%                                 % setup joystick
%                                 id = 1; % Note: may need to be changed if multiple joysticks present
%                                 joy = vrjoystick(id)
%                                 caps(joy) % display joystick information
%                                 duration = 30;  % Set duration of the simulation (seconds)
%                                 dt = 0.15;      % Set time step for simulation (seconds)
% 
%                                 n = 0;  % Initialise step count to zero 
%                                 tic;    % recording simulation start time
%                                 while( toc < duration)
% 
%                                     n=n+1; % increment step count
% 
%                                     % read joystick
%                                     [axes, buttons, povs] = read(joy);
%                                    if buttons(1) ~= 0
%                                        buttloop = 0;
%                                    end
%                                        if buttons(2) ~= 0
%                                        buttloop = 0;
%                                        end  
% 
%                                     % wait until loop time elapsed
%                                     if (toc > dt*n)
%                                         warning('Loop %i took too much time - consider increating dt',n);
%                                     end
%                                     while (toc < dt*n); % wait until loop time (dt) has elapsed 
%                                     end
%                                 end
                               pause(0.25);
                            if g.extra == 0%comment above in and this out for joystick
                                buttloop=0;
                            end
                            end
                            g.extra = 0;
                            extraOut = 0;
                            display(['Object removed, resuming motion'])
                        end
                    end
                end
            end
    end
end