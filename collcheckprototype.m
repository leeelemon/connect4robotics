%% line plane intersection code
%uses code from lab 5, questions 2 and 3, and my prep work for quiz 3
clear all 
close all
% Link('theta',__,'d',__,'a',__,'alpha',__,'offset',__,'qlim',[ ... ])

L1 = Link('d',0.103,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)])

L2 = Link('d',0,'a',0.14363,'alpha',0,'offset',pi/2,'qlim',[deg2rad(5), deg2rad(80)])

L3 = Link('d',0,'a',0.17063,'alpha',0,'offset',0,'qlim',[deg2rad(-5), deg2rad(190)])

L4 = Link('d',0,'a',0.05109,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)])

L5 = Link('d',0,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)])

dobot = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot')

dobot.tool = transl(0,0,-0.03);

qz = [0 0 0 0 0];

T = transl(0.3,0.3,0.1);

Q = ikcon(dobot,T);

% figure(2)
dobot.plot(Q)
%% get lines
trcol = zeros(4,4,dobot.n+1);
trcol(:,:,1) = dobot.base;
L = dobot.links;
for i = 1 : dobot.n
    trcol(:,:,i+1) = trcol(:,:,i) * trotz(Q(i)+L(i).offset) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotx(L(i).alpha)
end



%% create line points and normals for checking
pointOnPlane = [0,0.15,0.1];
planeNormal = [0,-1,0];
check = false;

for i = 1 : size(tr,3)-1
    [intersectionPoint, check] = LinePlaneIntersection(planeNormal,pointOnPlane,trcol(1:3,4,i)',trcol(1:3,4,i+1)');
    if check ==1
        if intersectionPoint(1)<0.25 && 0<intersectionPoint(1)
            if intersectionPoint(3)<0.18 && 0<intersectionPoint(3)
                display(['Collision at joint ', num2str(i), ' Global co-ord ', num2str(intersectionPoint(1)),',',num2str(intersectionPoint(2)),',',num2str(intersectionPoint(3)),'.' ' See plot.'])
                plot3(intersectionPoint(1),intersectionPoint(2),intersectionPoint(3), 'r.', 'MarkerSize', 50 );
            end
        end
    end
end
    



% point1OnLine = [0,5,0]
% point2OnLine = [0,0,7]
% intersectionPoint = [0,0,0]
% check = false;
% [intersectionPoint, check] = LinePlaneIntersection(planeNormal,pointOnPlane,tr(1:3,4,i)',tr(1:3,4,i+1)');

% boardlocX = -0.102
% boardlocY = 0.25
% boardlocZ = 0
% 
% pointOnPlane = [boardlocX,boardlocY,boardlocZ];
% %potential to implement code to automatically determine the gameboard moidels planeNormal
% planeNormal = [0,-1,0];