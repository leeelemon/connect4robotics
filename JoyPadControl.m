%% Robotics
% Lab 11 - Question 2 skeleton code
clc
%% setup joystick
id = 1; % Note: may need to be changed if multiple joysticks present
joy = vrjoystick(id)
caps(joy) % display joystick information


%% Set up robot
mdl_puma560;                    % Load Puma560
robot = p560;                   % Create copy called 'robot'
robot.tool = transl(0.1,0,0);   % Define tool frame on end-effector


%% Start "real-time" simulation
q = qn;                 % Set initial robot configuration 'q'

HF = figure(1);         % Initialise figure to display robot
robot.plot(q);          % Plot robot in initial configuration
robot.delay = 0.001;    % Set smaller delay when animating
set(HF,'Position',[0.1 0.1 0.8 0.8]);

duration = 60;  % Set duration of the simulation (seconds)
dt = 0.15;      % Set time step for simulation (seconds)

n = 0;  % Initialise step count to zero 
tic;    % recording simulation start time
while( toc < duration)
    
    n=n+1; % increment step count

    % read joystick
    [axes, buttons, povs] = read(joy);
       Q = [0, pi, 0, pi, 0, pi];
   if buttons(1) ~= 0
       robot.plot(Q);
   end
    F = [0, pi/2, 0, pi, 0, pi/2];
       if buttons(2) ~= 0
       robot.plot(F);
       end
   
   
   if axes(1) ~=0 || axes(2) ~=0 || axes(3) ~=0 || axes(4) ~=0 || axes(5) ~=0 || axes(6) ~=0
       Q = [axes(1)*pi axes(2)*pi axes(3)*pi axes(4)*pi axes(5)*pi axes(6)*pi];
       robot.plot(Q);
   end
    
    % Update plot
    %robot.animate(q);  
    
    % wait until loop time elapsed
    if (toc > dt*n)
        warning('Loop %i took too much time - consider increating dt',n);
    end
    while (toc < dt*n); % wait until loop time (dt) has elapsed 
    end
end
      
