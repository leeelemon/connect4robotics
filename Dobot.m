classdef Dobot < handle
    properties
        %> Robot model
        model;
%         L = log4matlab('Lab_Assignment_2.log'); 
        workspace = [-0.5 0.5 -0.4 0.6 -0.1 0.6];  
        stop = 0
    end
    
    methods%% Class for Sawyer robot simulation
function self = Dobot()

%> Define the boundaries of the workspace
      
% robot = 
self.GetDobotRobot();
% robot = 
zero = zeros(1,self.model.n);
self.ModelAndColourRobot(zero);%robot,workspace);
end

%% GetDobotRobot
% Given a name (optional), create and return a Dobot robot model
function GetDobotRobot(self)
%     if nargin < 1
        % Create a unique name (ms timestamp after 1ms pause)
        pause(0.001);
        name = ['Dobot',datestr(now,'yyyymmddTHHMMSSFFF')];
%     end
L1 = Link('d',0.103,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-135), deg2rad(135)])
L2 = Link('d',0,'a',0.14363,'alpha',0,'offset',0,'qlim',[deg2rad(5), deg2rad(80)])
L3 = Link('d',0,'a',0.17063,'alpha',0,'offset',0,'qlim',[deg2rad(-80), deg2rad(5)])
L4 = Link('d',0,'a',0.05109,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90), deg2rad(90)])
L5 = Link('d',0,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-85), deg2rad(85)])

self.model = SerialLink([L1,L2,L3,L4,L5], 'name', 'DoBot')
self.model.tool = transl(0,0,0.06);
end
% r.base = trotz(-pi/2) * trotx(pi);

% tr_x_p90 = makehgtform('xrotate',pi/2); tr_x_p90 = tr_x_p90(1:3,1:3);
% tr_y_p90 = makehgtform('yrotate',pi/2); tr_y_p90 = tr_y_p90(1:3,1:3);
% tr_z_p90 = makehgtform('zrotate',pi/2); tr_z_p90 = tr_z_p90(1:3,1:3);
% 
% tr_x_n90 = makehgtform('xrotate',-pi/2); tr_x_n90 = tr_x_n90(1:3,1:3);
% tr_y_n90 = makehgtform('yrotate',-pi/2); tr_y_n90 = tr_y_n90(1:3,1:3);
% tr_z_n90 = makehgtform('zrotate',-pi/2); tr_z_n90 = tr_z_n90(1:3,1:3);
% 
% % arm = load('UR_10_lowResolution.mat');
% 
% % for i = 1: r.n+1
% %     r.faces{i} = arm.shapeModel(i).face;
% %     r.points{i} = arm.shapeModel(i).vertex;
% % end
% 
% r.delay = 0;
% 
% % Check if there is a robot with this name, otherwise plot one 
% % if isempty(findobj('Tag', r.name))
% %     r.plot3d([0,0,pi/2,0,0,0]);
% %     if isempty(findobj(get(gca,'Children'),'Type','Light'))
% %         camlight
% %     end
% % end
% 
% r.plot([0,0,0,0,0,0],'scale',0.6,'nowrist');
% 
% r.delay = 0;
% % handles = findobj('Tag', r.name);
% % h = get(handles,'UserData');
% % h.link(1).Children.FaceColor = [0.7,0.7,0.7];
% % h.link(2).Children.FaceColor = [1, 1, 0.8];
% % h.link(3).Children.FaceColor = [1, 1, 0.9];
% % h.link(4).Children.FaceColor = [1, 1, 0.9];
% % h.link(5).Children.FaceColor = [0.8,0.8,0.8];
% % h.link(6).Children.FaceColor = [0.8,0.8,0.8];
% % h.link(7).Children.FaceColor = [0.8,0.8,0.8];

%% ModelAndColourRobot
% Given a robot index, add the glyphs (vertices and faces) and
% colour them in if data is available
% ply files are called Dobot0, Dobot1 according to link index
function ModelAndColourRobot(self, position)%robot,workspace)
    for linkIndex = 0:self.model.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread([num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
        self.model.faces{linkIndex+1} = faceData;
        self.model.points{linkIndex+1} = vertexData;
    end

    % Display robot
%     self.model.plot3d(position,'noarrow','workspace',self.workspace,'nojaxes');
    if isempty(findobj(get(gca,'Children'),'Type','Light'))
        camlight
    end  
    self.model.delay = 0;

   % Try to correctly colour the arm (if colours are in ply file data)
    for linkIndex = 0:self.model.n
        handles = findobj('Tag', self.model.name);
        h = get(handles,'UserData');
        try 
%             h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
%                                                           , plyData{linkIndex+1}.vertex.green ...
%                                                           , plyData{linkIndex+1}.vertex.blue]/255;
%             h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
    end
    try 
        self.L.mlog = {self.L.DEBUG,'Dobot',['The transform of ', self.model.name, 'is: ',self.L.MatrixToString(self.model.fkine(self.model.theta))]};  
    catch ME_1
        disp(ME_1);
    end
end    
%% Animate Robot
function Plot(self,q,msg)
try delete(text_h);end;   
self.model.plot3d(q,'noarrow','workspace',self.workspace,'nojaxes', 'fps', 70);
drawnow();
%% find tool transform and log in file
Tr = self.model.fkine(q);
self.L.mlog = {self.L.DEBUG,'Sawyer',msg};
self.L.mlog = {self.L.DEBUG,'Sawyer',['The transform of ', self.model.name, 'is: ', self.L.MatrixToString(Tr)]};           
end
    end
    
end

