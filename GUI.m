function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 14-Oct-2017 16:16:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in EPO.
function EPO_Callback(hObject, eventdata, handles)
global dobot
if (dobot.stop == 1)
    handles.text14.String = '';
    dobot.stop = 0;
else
    handles.text14.String = 'Emergency Stop Activated';
    dobot.stop = 1;
end
% hObject    handle to EPO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function J1Slider_Callback(hObject, eventdata, handles)
% hObject    handle to J1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
pos = zeros(1,5);
pos(1) = deg2rad(get(hObject,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));
dobot.model.animate(pos);

set(handles.J1Text,'String',num2str(get(hObject,'Value')));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function J1Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function J2Slider_Callback(hObject, eventdata, handles)
% hObject    handle to J2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(hObject,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));
dobot.model.animate(pos);

set(handles.J2Text,'String',num2str(get(hObject,'Value')));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function J2Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function J3Slider_Callback(hObject, eventdata, handles)
% hObject    handle to J3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(hObject,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));
dobot.model.animate(pos);

set(handles.J3Text,'String',num2str(get(hObject,'Value')));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function J3Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function J5Slider_Callback(hObject, eventdata, handles)
% hObject    handle to J5Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(hObject,'Value'));

dobot.model.animate(pos);

set(handles.J5Text,'String',num2str(get(hObject,'Value')));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function J5Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J5Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in StartButton.
function StartButton_Callback(hObject, eventdata, handles)
% hObject    handle to StartButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% clear all; close all; clf; clc;
    global g;
    g.ir = 1; %incrementer for red checkers inside holder
    g.iy = 1; %incrementer for yellow checkers inside holder
    g.dist = 0.034;
    g.boardLoc = transl(0.0,0.26,0);
    g.extra = 0;
    g.fingNum=0;
    SetupGame(hObject, eventdata, handles);
    for n=1:g.numGames
        SetUpRobot(hObject, eventdata, handles);
        InitBoard(hObject, eventdata, handles);
        PlotPly(hObject, eventdata, handles);
        ShowBoard(hObject, eventdata, handles);
        while ~g.done
            GetMove(hObject, eventdata, handles);
            CheckWin(hObject, eventdata, handles);
            ShowBoard(hObject, eventdata, handles);
        end 
        pause(1)
    end


function SetUpRobot(hObject, eventdata, handles)
global g;
global dobot
dobot = Dobot;
qz = [0 0 0 0 0];
T = transl(0,0,0.1);
axes(handles.axes1);
dobot.model.plot3d(qz, 'workspace',dobot.workspace);
hold on;

% Used to see workspace of DoBot
% poseCount = 1;
% for k = 135:-10:45
%     q1 = k*pi/180;
%     for i=5:10:80 %joint 1 angle limits -170 to 170 
%         q2 = i*pi/180; 
%         for j=-80:10:5 %joint 2 angle limits -90 to 135 
%             q3 = j*pi/180;
%             q4 = (q2+q3)*-1;
%             q = [q1 q2 q3 q4 0];
%             pos = dobot.model.fkine(q);
%             posArray(poseCount,:) = [pos(1:3,4)]; 
%             poseCount = poseCount +1; 
%             dobot.model.animate(q);
%         
%         end
%     end
% end

% for i = 1:poseCount-1
%     plot3(posArray(i,1), posArray(i,2), posArray(i,3), '.r');
% end

% Finds the required drop position and pick position and calls PlotTraj to
% move the DoBot
function DobotTurn(r,c,hObject, eventdata, handles)
r;
c;
global g; 
global dobot;
offset = g.dist;
switch c 
    case 1
       dropPosition =  transl(g.boardLoc(1,4), g.boardLoc(2,4), 0.195) *trotx(pi);
    case 2
       dropPosition =  transl(g.boardLoc(1,4) + offset, g.boardLoc(2,4), 0.195) *trotx(pi);
    case 3
       dropPosition = transl(g.boardLoc(1,4) + 2*offset, g.boardLoc(2,4), 0.195) *trotx(pi);
    case 4 
        dropPosition = transl(g.boardLoc(1,4) + 3*offset, g.boardLoc(2,4), 0.195) *trotx(pi);
    case 5 
        dropPosition = transl(g.boardLoc(1,4) + 4*offset, g.boardLoc(2,4), 0.195) *trotx(pi);
    case 6 
        dropPosition = transl(g.boardLoc(1,4) + 5*offset, g.boardLoc(2,4), 0.195) *trotx(pi);
    case 7 
        dropPosition = transl(g.boardLoc(1,4) + 6*offset, g.boardLoc(2,4), 0.195) *trotx(pi);
end 
dropPose = dobot.model.ikcon(dropPosition);
currentPose = dobot.model.getpos;
if g.player == 2
    pickPosition = transl(g.redholder(1,4) + g.checkerloc(g.ir,g.ir,1), g.redholder(2,4) + g.checkerloc(g.ir,g.ir,2), g.redholder(3,4) + 0.025) * trotz(pi/2);
    g.ir = g.ir + 1;
else 
    pickPosition = transl(g.yellowholder(1,4) + g.checkerloc(g.iy,g.iy,1), g.yellowholder(2,4) + g.checkerloc(g.iy,g.iy,2), g.redholder(3,4) + 0.025) * trotz(pi/2);
    g.iy = g.iy + 1;
end

pickPose = dobot.model.ikcon(pickPosition * trotx(pi));
%profile on
PlotTraj(currentPose,pickPose,hObject, eventdata, handles)
%profile viewer

%drops down over the checker
currentPose = dobot.model.getpos
gripPose = dobot.model.ikcon(pickPosition * transl(0,0,-0.01) * trotx(pi));
PlotTraj(currentPose, gripPose, hObject, eventdata, handles)

%to the middle position
currentPose = dobot.model.getpos
interPosition = transl(g.boardLoc(1,4) + 6*offset, g.boardLoc(2,4) - 0.06, 0.200) * trotx(pi);
interPose = dobot.model.ikcon(interPosition)
PlotTraj(currentPose,interPose,hObject, eventdata, handles)

%to do drop position
currentPose = dobot.model.getpos;
PlotTraj(currentPose,dropPose,hObject, eventdata, handles)

% back to the middle position
%Test = dobot.model.fkine(dobot.model.getpos);
currentPose = dobot.model.getpos;
PlotChecker(r,c,hObject, eventdata, handles);
PlotTraj(currentPose, interPose,hObject, eventdata, handles)

function SetupGame(hObject, eventdata, handles)
global g;
prompts = {'Player 1''s name','Player 2''s name','# rows','# columns','connect # to win','# games'};
inp = {'Yellow','*Red','6','7','4','1'};
inp = inputdlg(prompts,'Connect X',1,inp);
if isempty(inp); error('User canceled input'); end;
g.name{1}=inp{1};           % Player 1
g.name{2}=inp{2};           % Player 2
g.nr=str2double(inp{3});    % # of rows
g.nc=str2double(inp{4});    % # of columns
g.n=str2double(inp{5});     % connect 4 or 3 or 2?
g.numGames=str2double(inp{6});  
g.playercolors={'y','r'}; % yellow,red
g.results = [0 0 0];
 
function InitBoard(hObject, eventdata, handles)
global g;
g.board = zeros(g.nr,g.nc); % playing board
g.player = 1; % current player
g.done = 0;
g.winner = 0;
g.lastmove = 0;
%checker positions in model
%checkerpose stores all the poses of the checkers positions for modelling
%purposes
%Bottom left is 0,0
dist = g.dist; % distance between centre of holes 0.033
g.checkerpose = ones(6,7,2);
x = 0;
z = 0;
for n=6:-1:1
    x = 0;
    for m=1:1:7   
        g.checkerpose(n,m,1) = g.boardLoc(1,4) + (x) * dist;
        g.checkerpose(n,m,2) = g.boardLoc(3,4) + (z) * dist;
        x = x + 1;   
    end
    z = z + 1; 
end
g.checkerpose = flipud(g.checkerpose);   %I flip it horizontally because the connect4 code stores the board differently

% Plots all the models required in the simulation
function PlotPly (hObject, eventdata, handles)
global g;
%% Plot the ply model board 
axes(handles.axes1);
[fboard,vboard,databoard] = plyread('board.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.boardLoc(1,4),vboard(:,2) + g.boardLoc(2,4), vboard(:,3) + g.boardLoc(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
camlight;
hold on;

%% Plot the box
axes(handles.axes1);
% [fbox,vbox,databox] = plyread('WIREBOX.ply','tri');
% % Scale the colours to be 0-to-1 (they are originally 0-to-255
% vertexColours = [databox.vertex.red, databox.vertex.green, databox.vertex.blue] / 255;
% % Then plot the trisurf
% boxMesh_h = trisurf(fbox,vbox(:,1) + g.boardLoc(1,4)+0.55,vbox(:,2) + g.boardLoc(2,4)-0.45, vbox(:,3) + g.boardLoc(3,4)-0.025 ...
%     ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

%% Plot Alien
if g.extra == 1
    [falien,valien,dataalien] = plyread('alien.ply','tri');
    % Scale the colours to be 0-to-1 (they are originally 0-to-255
    vertexColours = [dataalien.vertex.red, dataalien.vertex.green, dataalien.vertex.blue] / 255;
    % Then plot the trisurf
    g.alienMesh_h = trisurf(falien,valien(:,1) + g.boardLoc(1,4)+0.1,valien(:,2) + g.boardLoc(2,4)-0.15, valien(:,3) + g.boardLoc(3,4)+0.1 ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
end
%% Checker locations
checkerloc = zeros(7,3,2);
checkerloc(:,:,1) = [3.7,3.7,3.7;16.2,16.2,16.2;28.7,28.7,28.7;41.2,41.2,41.2;53.7,53.7,53.7;66.2,66.2,66.2;78.7,78.7,78.7];
checkerloc(:,:,2) = [17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;17.7,51.9,86.1;];
g.checkerloc = checkerloc * 0.001;
%% Plot the checkers
% Yellow Holder
g.yellowholder = transl(0.2,0.03,0) * trotz(deg2rad(90)); % pick a location later
[fboard,vboard,databoard] = plyread('checkerholder.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.yellowholder(1,4),vboard(:,2) + g.yellowholder(2,4), vboard(:,3) + g.yellowholder(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

% Red Holder
g.redholder = transl(0.2,-0.1,0) * trotz(deg2rad(90)); % pick a location later
[fboard,vboard,databoard] = plyread('checkerholder.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [databoard.vertex.red, databoard.vertex.green, databoard.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fboard,vboard(:,1) + g.redholder(1,4),vboard(:,2) + g.redholder(2,4), vboard(:,3) + g.redholder(3,4) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

% red checkers
[fchecker,vchecker,datachecker] = plyread('redchecker.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
for n=1:1:7
    for m=1:1:3 
        boardMesh_h = trisurf(fchecker,vchecker(:,1) + g.redholder(1,4) + g.checkerloc(n,m,1) ,vchecker(:,2) + g.redholder(2,4) + g.checkerloc(n,m,2) , vchecker(:,3) + g.redholder(3,4) ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
    end
end

%yellow checkers
[fchecker,vchecker,datachecker] = plyread('yellowchecker.ply','tri');
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
for n=1:1:7
    for m=1:1:3 
        boardMesh_h = trisurf(fchecker,vchecker(:,1) + g.yellowholder(1,4) + g.checkerloc(n,m,1) ,vchecker(:,2) + g.yellowholder(2,4) + g.checkerloc(n,m,2) , vchecker(:,3) + g.yellowholder(3,4) ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
    end
end

function ShowBoard(hObject, eventdata, handles)
axes(handles.axes2);
global g;
if g.lastmove==0 % board initialization
    %clf reset
    axis([0 g.nc+1 0 g.nr+1]);
    hold on
    bc='k-';
    plot([.5 g.nc+.5],[.5 .5],bc); % bottom
    for i=0:g.nc
        plot([i+.5 i+.5],[.5 g.nr+.5],bc); % vertical lines
    end
else % plot last move
    [r,c]=ind2sub(size(g.board),g.lastmove);
    i = g.board(r,c);
    plot(c,r,'o','markersize',250/max(g.nr,g.nc),'markeredgecolor','black','markerfacecolor',g.playercolors{i});
end
if g.winner>0
    plot(g.win_x,g.win_y,'k-','linewidth',5,'color','green');
    txt = sprintf('%s wins',g.name{g.winner});
    UpdateResults(g.winner);
%     playdonesound;
elseif g.done>0
    txt = sprintf('Tie game!');
    UpdateResults(3);
%     playdonesound;
else
    txt = sprintf('%s''s turn',g.name{g.player});
end
title(sprintf('Connect %u : %s',g.n,txt),'fontsize',16);
drawnow;
 
function [r,c] = GetMove(hObject, eventdata, handles)
global g;
goodmove = 0;
while ~goodmove
    if strncmp(g.name{g.player},'*',1)
        c = GetComputerMove;
    else
        [x,y] = ginput(1);
        c = round(x);
    end
    if c<1 || c>g.nc || g.board(g.nr,c)>0
        beep;
        continue;
    end
    goodmove=1;
    r=find(g.board(:,c)==0);
    g.board(r(1),c) = g.player;
    %call function to pick up checker and drop it here
    DobotTurn(r,c,hObject, eventdata, handles); 
    g.lastmove = sub2ind(size(g.board),r(1),c);
    g.player=mod(g.player,2)+1;
end
 
function CheckWin(hObject, eventdata, handles)
global g;
offs(1) = g.nr; % right
offs(2) = 1; % up
offs(3) = g.nr+1; %up, right
offs(4) = -g.nr+1; %up, left
for s=1:g.nr*g.nc
    if g.board(s)>0
        [r,c]=ind2sub(size(g.board),s);
        for o=1:length(offs)
            if o>1 && r>(g.nr-g.n+1); continue; end; % don't do upward check
            if (o==1 || o==3) && c>(g.nc-g.n+1); continue; end; % don't do rightward check
            if o==4 && c<g.n; continue; end; % don't do leftward check
            i=(0:g.n-1)*offs(o)+ s;
            if isempty(find(i<1, 1)) && isempty(find(i>g.nr*g.nc, 1))
                t=g.board(i);
                if t(:)==t(1)
                    g.winner=t(1);
                    g.done=1;
                    [r1 c1] = ind2sub(size(g.board),i(1));
                    [r2 c2] = ind2sub(size(g.board),i(g.n));
                    g.win_x = [c1 c2];
                    g.win_y = [r1 r2];
                    return
                end
            end
        end
    end
end
g.done = ( nnz(g.board)==(g.nr*g.nc) );
 
% function playdonesound
% global g;
% if sum(g.results)<g.numGames; return; end
% builtinsounds = {'train', 'gong', 'chirp', 'handel', 'laughter', 'splat'};
% i=randi(length(builtinsounds));
% load(builtinsounds{i});
% wavplay(y,Fs,'async')
 
function UpdateResults(p,hObject, eventdata, handles)
global g;
g.results(p)=g.results(p)+1;
fprintf('%s wins: %u\n%s wins: %u\nTies:%u\n',g.name{1},g.results(1),g.name{2},g.results(2),g.results(3));

% Simple computer move, decides on column to select
function c = GetComputerMove
global g;
o = find(g.board(g.nr,:)==0);
i = randi(length(o));
c = o(i);

% Plots the 3D PLY checker inside the gameboard
function PlotChecker(r,c,hObject, eventdata, handles)
global g;
axes(handles.axes1);
if g.player == 2
    [fchecker,vchecker,datachecker] = plyread('redchecker.ply','tri');
else 
    [fchecker,vchecker,datachecker] = plyread('yellowchecker.ply','tri');
end
% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [datachecker.vertex.red, datachecker.vertex.green, datachecker.vertex.blue] / 255;
% Then plot the trisurf
boardMesh_h = trisurf(fchecker,vchecker(:,2) + g.checkerpose(r(1),c,1) ,vchecker(:,1) + g.boardLoc(2,4) , vchecker(:,3) + g.checkerpose(r(1),c,2) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
%how do i rotate these checkers??
drawnow;



function J1Text_Callback(hObject, eventdata, handles)
% hObject    handle to J1Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot; 
pos = zeros(1,5);
pos(1) = deg2rad(str2double(get(hObject,'String')))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));

set(handles.J1Slider,'Value', str2double(get(hObject,'String')));

dobot.model.animate(pos);

% Hints: get(hObject,'String') returns contents of J1Text as text
%        str2double(get(hObject,'String')) returns contents of J1Text as a double


% --- Executes during object creation, after setting all properties.
function J1Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J1Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function J2Text_Callback(hObject, eventdata, handles)
% hObject    handle to J2Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot; 
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(str2double(get(hObject,'String')));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));

set(handles.J2Slider,'Value', str2double(get(hObject,'String')));

dobot.model.animate(pos);
% Hints: get(hObject,'String') returns contents of J2Text as text
%        str2double(get(hObject,'String')) returns contents of J2Text as a double


% --- Executes during object creation, after setting all properties.
function J2Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J2Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function J3Text_Callback(hObject, eventdata, handles)
% hObject    handle to J3Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot; 
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(str2double(get(hObject,'String')))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(handles.J5Slider,'Value'));

set(handles.J3Slider,'Value', str2double(get(hObject,'String')));

dobot.model.animate(pos);
% Hints: get(hObject,'String') returns contents of J3Text as text
%        str2double(get(hObject,'String')) returns contents of J3Text as a double


% --- Executes during object creation, after setting all properties.
function J3Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J3Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function J5Text_Callback(hObject, eventdata, handles)
% hObject    handle to J5Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of J5Text as text
%        str2double(get(hObject,'String')) returns contents of J5Text as a double

global dobot; 
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(str2double(get(hObject,'String')));

set(handles.J5Slider,'Value', str2double(get(hObject,'String')));

dobot.model.animate(pos);
% --- Executes during object creation, after setting all properties.
function J5Text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to J5Text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XText_Callback(hObject, eventdata, handles)
% hObject    handle to XText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XText as text
%        str2double(get(hObject,'String')) returns contents of XText as a double


% --- Executes during object creation, after setting all properties.
function XText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YText_Callback(hObject, eventdata, handles)
% hObject    handle to YText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YText as text
%        str2double(get(hObject,'String')) returns contents of YText as a double


% --- Executes during object creation, after setting all properties.
function YText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ZText_Callback(hObject, eventdata, handles)
% hObject    handle to ZText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ZText as text
%        str2double(get(hObject,'String')) returns contents of ZText as a double


% --- Executes during object creation, after setting all properties.
function ZText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ZText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in spawnAlien.
function spawnAlien_Callback(hObject, eventdata, handles)
% hObject    handle to spawnAlien (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global g;
display(['alienNNNNNNN'])
if g.extra==1
    g.extra=0;
    axes(handles.axes1)
    try delete(g.alienMesh_h);
    end
else
    g.extra=1;
end


% --- Executes on button press in spawnFinger.
function spawnFinger_Callback(hObject, eventdata, handles)
% hObject    handle to spawnFinger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global g;
if g.fingNum==0
    g.fingNum=69;
else g.fingNum ~=0
    g.fingNum=0;
end


% --- Executes on button press in teach.
function teach_Callback(hObject, eventdata, handles)
% hObject    handle to teach (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot; 
dobot = Dobot;
pos = zeros(1,5);
pos(1) = deg2rad(get(handles.J1Slider,'Value'))*-1;
pos(2) = deg2rad(get(handles.J2Slider,'Value'));
pos(3) = deg2rad(get(handles.J3Slider,'Value'))*-1;
pos(4) = (pos(2)+pos(3))*-1;
pos(5) = deg2rad(get(hObject,'Value'));

set(handles.J1Text, 'String', num2str(get(handles.J1Slider, 'Value')));
set(handles.J2Text, 'String', num2str(get(handles.J2Slider, 'Value')));
set(handles.J3Text, 'String', num2str(get(handles.J3Slider, 'Value')));
set(handles.J5Text, 'String', num2str(get(handles.J5Slider, 'Value')));


axes(handles.axes1);
camlight;
dobot.model.plot3d(pos, 'workspace', dobot.workspace);
hold on;


% --- Executes on button press in cartesianGo.
function cartesianGo_Callback(hObject, eventdata, handles)
% hObject    handle to cartesianGo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
X = str2double(get(handles.XText,'String'));
Y = str2double(get(handles.YText,'String'));
Z = str2double(get(handles.ZText,'String'));

pos = transl(X,Y,Z)*trotx(pi);
joints = dobot.model.ikcon(pos)
path = jtraj(dobot.model.getpos, joints, 30);
for i = 1:length(path)
    dobot.model.animate(path(i,1:5));
end

% --- Executes on button press in JoyStick.
function JoyStick_Callback(hObject, eventdata, handles)
% hObject    handle to JoyStick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot;
axes(handles.axes1);
%set up joystick
id = 1; 
joy = vrjoystick(id)
caps(joy) % display joystick information

%setup robot
dobot = Dobot;
q = [0 0 0 0 0];
%start real time simulation 
%axes(handles.axes1);

dobot.model.plot3d(q, 'workspace', dobot.workspace);
hold on;
dobot.model.delay = 0.001;

duration = 360; 
dt = 0.25; 

n = 0 
tic; 
while( toc < duration)
    
    n=n+1; % increment step count

    % read joystick
    [axis, buttons, povs] = read(joy);

   %set offset for joint limits
   offset1 = deg2rad(135)*-1;
   offset2 = deg2rad(37.5);
   offset3 = deg2rad(42.5)*-1;
   offset5 = deg2rad(85);
   
   if axis(1) ~=0 || axis(2) ~=0 || axis(3) ~=0 || axis(4) ~=0 || axis(5) ~=0
       Q = [axis(1)*offset1, axis(2)*offset2+deg2rad(42.5), axis(3)*offset3-deg2rad(37.5), 0, axis(6)*offset5];
       a = Q(2);
       b = Q(3);
       Q(4) = (a+b)*-1;
       dobot.model.animate(Q);
   end
    
    % Update plot
    %robot.animate(q);  
    
    % wait until loop time elapsed
    if (toc > dt*n)
        warning('Loop %i took too much time - consider increating dt',n);
    end
    while (toc < dt*n); % wait until loop time (dt) has elapsed 
    end
end

% Plots the trajectory while checking for EPO, light curtain, and alien
% Calls colCheck every step to check for any collisions
function PlotTraj (Pose1,Pose2,hObject,eventdata,handles)
global g
global dobot
pointOnPlane = [g.boardLoc(1,4),g.boardLoc(2,4),g.boardLoc(3,4)];
planeNormal = [0,-1,0];
path = jtraj(Pose1, Pose2, 30)
for i = 1:length(path)   
     %collision checking     
     if g.extra == 1
         axes(handles.axes1);
         [falien,valien,dataalien] = plyread('alien.ply','tri');
         % Scale the colours to be 0-to-1 (they are originally 0-to-255
         vertexColours = [dataalien.vertex.red, dataalien.vertex.green, dataalien.vertex.blue] / 255;
         % Then plot the trisurf
         g.alienMesh_h = trisurf(falien,valien(:,1) + g.boardLoc(1,4)+0.1,valien(:,2) + g.boardLoc(2,4)-0.15, valien(:,3) + g.boardLoc(3,4)+0.1 ...
             ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
         drawnow;
     end
     
     [colCheckOutput, extraOut] = colCheck(pointOnPlane, planeNormal, i, path, dobot, g);
     
     if g.fingNum == 69
         axes(handles.axes1);
         [ffinger,vfinger,datafinger] = plyread('finger1.ply','tri');
         % Scale the colours to be 0-to-1 (they are originally 0-to-255
        vertexColours = [datafinger.vertex.red, datafinger.vertex.green, datafinger.vertex.blue] / 255;
        % Then plot the trisurf
        g.fingerMesh_h = trisurf(ffinger,vfinger(:,2) + 0,vfinger(:,1) + 0.5, vfinger(:,3) + 0 ...
        ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
        drawnow;
        display(['Perimeter Breach'])
        display(['  Perimeter Breach'])
        display(['    Perimeter Breach'])
        display(['  Perimeter Breach'])
        display(['Perimeter Breach'])
        while g.fingNum==69
            pause(0.5);
        end
        try delete(g.fingerMesh_h);
        end
     end
    
    dobot.model.animate(path(i,1:5));
    drawnow;
    
    while(dobot.stop == 1)
        pause(1)
    end
end